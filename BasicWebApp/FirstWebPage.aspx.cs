﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BasicWebApp
{
    public partial class FirstWebPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, System.EventArgs e)
        {
            // TextBox1.Text = TextBox1.Text + ", welcome to Visual Studio!";

            string teks_operasi = TextBox1.Text;
            string expr = "\\d+";

            //System.Diagnostics.Debug.WriteLine("Teks: " + teks_operasi);

            //int[] kumpulan_bilangan = new int[10];

            TextBox1.Text = Hitung(teks_operasi, 0).ToString();

        }

        protected void tampilkanPesankeUI(string pesan)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload=function(){");
            sb.Append("alert('");
            sb.Append(pesan);
            sb.Append("')};");
            sb.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
        }

        protected double Hitung(string teks_hitungan, int level)
        {
            double hasil_perhitungan = 0;

            string teks_hitungan_diproses = teks_hitungan;
            string teks_hitungan_disederhanakan = "";

            while (teks_hitungan_diproses.Length > 0)
            {

                //LogOpsional(teks_hitungan_diproses);

                int ndx = 0;
                int firstParenthesis = teks_hitungan_diproses.IndexOf("(", StringComparison.Ordinal);
                int balance = 1;
                int lastParenthesis = 0;

                while (ndx < teks_hitungan_diproses.Length)
                {
                    if (ndx == firstParenthesis)
                    {
                        ndx++;
                        continue;
                    }
                    if (teks_hitungan_diproses[ndx] == '(')
                        balance++;
                    if (teks_hitungan_diproses[ndx] == ')')
                        balance--;
                    if (balance == 0)
                    {
                        lastParenthesis = ndx;
                        break;
                    }
                    ndx++;
                }

                //LogOpsional("First Parenthesis: " + firstParenthesis);
                //LogOpsional("Last Parenthesis: " + lastParenthesis);

                if (balance == 0)
                {
                    string operasi_prioritas_paling_luar = teks_hitungan_diproses.Substring(firstParenthesis + 1,
                        lastParenthesis - firstParenthesis - 1);
                    LogOpsional(operasi_prioritas_paling_luar);

                    teks_hitungan_disederhanakan += teks_hitungan_diproses.Substring(0, firstParenthesis);

                    teks_hitungan_disederhanakan += Hitung(operasi_prioritas_paling_luar, level + 1).ToString();

                    // utk pemrosesan selanjutnya

                    teks_hitungan_diproses = teks_hitungan_diproses.Substring(lastParenthesis);

                    if (teks_hitungan_diproses.Length > 0)
                    {
                        teks_hitungan_diproses = teks_hitungan_diproses.Remove(0, 1);
                    }
                }
                else
                {
                    teks_hitungan_disederhanakan += teks_hitungan_diproses;
                    teks_hitungan_diproses = "";
                }

            }

            LogOpsional("Teks Hitungan Disederhanakan");
            LogOpsional(teks_hitungan_disederhanakan);

            MatchCollection koleksi_pencocokan_bilangan = Regex.Matches(teks_hitungan_disederhanakan, @"\d+");
            MatchCollection koleksi_pencocokan_operasi_aritmatika = Regex.Matches(teks_hitungan_disederhanakan, @"[\+\-\*\:\/]");

            var arr_pencocokan_bilangan = koleksi_pencocokan_bilangan.OfType<Match>().Select(m => m.Value).ToArray();
            var arr_pencocokan_operasi_aritmatika = koleksi_pencocokan_operasi_aritmatika.OfType<Match>().Select(m => m.Value).ToArray();

            int banyak_bilangan_cocok = arr_pencocokan_bilangan.Length;
            int banyak_operator_aritmatika = arr_pencocokan_operasi_aritmatika.Length;

            if (banyak_bilangan_cocok != banyak_operator_aritmatika + 1)
            {
                string message = "Cek lagi masukkannya. Ada yang salah!";

                tampilkanPesankeUI(message);

                return 0;
            }
            else
            {
                int indeks_iter = 0;
                //LogOpsional("Array Match: ");

                while (indeks_iter < banyak_bilangan_cocok)
                {
                    //LogOpsional(arr_pencocokan_bilangan[indeks_iter]);

                    if (indeks_iter > 0)
                    {
                        //LogOpsional(arr_pencocokan_operasi_aritmatika[indeks_iter]);

                        if (arr_pencocokan_operasi_aritmatika[indeks_iter - 1].Equals("+"))
                        {
                            hasil_perhitungan += double.Parse(arr_pencocokan_bilangan[indeks_iter]);
                        }
                        else if (arr_pencocokan_operasi_aritmatika[indeks_iter - 1].Equals("-"))
                        {
                            hasil_perhitungan -= double.Parse(arr_pencocokan_bilangan[indeks_iter]);
                        }
                        else if (arr_pencocokan_operasi_aritmatika[indeks_iter - 1].Equals("*"))
                        {
                            hasil_perhitungan *= double.Parse(arr_pencocokan_bilangan[indeks_iter]);
                        }
                        else if (arr_pencocokan_operasi_aritmatika[indeks_iter - 1].Equals("/") ||
                            arr_pencocokan_operasi_aritmatika[indeks_iter - 1].Equals(":"))
                        {
                            hasil_perhitungan /= double.Parse(arr_pencocokan_bilangan[indeks_iter]);
                        }
                    }
                    else
                    {
                        hasil_perhitungan += double.Parse(arr_pencocokan_bilangan[indeks_iter]);
                    }

                    indeks_iter++;
                }


                LogOpsional("Hasil Perhitungan: ");
                LogOpsional(hasil_perhitungan.ToString());
            }

            return hasil_perhitungan;
        }

        //protected void salah()
        //{
        //    int ndx = 0;
        //    int[] openOuterParenthesis = new int[10];
        //    int[] closedOuterParenthesis = new int[10];
        //    openOuterParenthesis[0] = teks_hitungan.IndexOf("(", StringComparison.Ordinal);
        //    closedOuterParenthesis[0] = 0;
        //    int balance = 1;
        //    int indeks_pasangan_kurung = 0;

        //    while (ndx < teks_hitungan.Length)
        //    {
        //        if (ndx == openOuterParenthesis[indeks_pasangan_kurung])
        //        {
        //            ndx++;
        //            continue;
        //        }
        //        if (teks_hitungan[ndx] == '(')
        //        {
        //            if (balance == -1)
        //            {
        //                openOuterParenthesis[indeks_pasangan_kurung] = ndx;
        //                balance = 0;
        //            }
        //            balance++;

        //        }
        //        if (teks_hitungan[ndx] == ')')
        //            balance--;
        //        if (balance == 0)
        //        {
        //            closedOuterParenthesis[indeks_pasangan_kurung] = ndx;
        //            indeks_pasangan_kurung++;
        //            balance = -1;
        //        }
        //        ndx++;
        //    }

        //    int banyak_pasangan_kurung = indeks_pasangan_kurung + 1;

        //    indeks_pasangan_kurung = 0;

        //    LogOpsional(teks_hitungan);

        //    while (indeks_pasangan_kurung < banyak_pasangan_kurung)
        //    {

        //        string operasi_prioritas_paling_luar = teks_hitungan.Substring(openOuterParenthesis[indeks_pasangan_kurung],
        //            closedOuterParenthesis[indeks_pasangan_kurung] - openOuterParenthesis[indeks_pasangan_kurung] + 1);
        //        LogOpsional(operasi_prioritas_paling_luar);

        //        indeks_pasangan_kurung++;
        //    }
        //}

        protected void salah2()
        {
            //Regex operasiPrioritasRegex = new Regex(@"(?<=\()(?:\([^()]*\)|[^()])*(?=\))");
            ////MatchCollection mc = operasiPrioritasRegex.Matches(teks_hitungan);

            //while (operasiPrioritasRegex.Match(teks_hitungan).Success)
            //{
            //    string operasi_prioritas_paling_luar = operasiPrioritasRegex.Match(teks_hitungan).Value;
            //    //int panjang_teks_dalam_kurung = operasi_prioritas_paling_luar.length;
            //    //string teks_dalam_kurung = operasi_prioritas_paling_luar.substring(1, panjang_teks_dalam_kurung-2);
            //    //logopsional(teks_dalam_kurung);

            //    LogOpsional("Level: " + level);
            //    LogOpsional(operasi_prioritas_paling_luar);

            //    string hasil_perhitungan_dalam_kurung = Hitung(operasi_prioritas_paling_luar, level + 1).ToString();

            //}
        }

        protected void LogOpsional(string teks)
        {
            StreamWriter sw = new StreamWriter(Server.MapPath("Logs/LogVariable"), true);
            sw.WriteLine(teks);
            sw.Flush();
            sw.Close();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "0";

            }
            else
            {
                TextBox1.Text += " 0";
            }
        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            TextBox1.Text += " /";
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "1";

            }
            else
            {
                TextBox1.Text += " 1";
            }
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "2";

            }
            else
            {
                TextBox1.Text += " 2";
            }
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "3";

            }
            else
            {
                TextBox1.Text += " 3";
            }
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "4";

            }
            else
            {
                TextBox1.Text += " 4";
            }
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "5";

            }
            else
            {
                TextBox1.Text += " 5";
            }
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "6";

            }
            else
            {
                TextBox1.Text += " 6";
            }
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "7";

            }
            else
            {
                TextBox1.Text += " 7";
            }
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "8";

            }
            else
            {
                TextBox1.Text += " 8";
            }
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            Regex regexAkhirTeksPerhitungan = new Regex(@"\d+\Z");
            if (regexAkhirTeksPerhitungan.Match(TextBox1.Text).Success)
            {
                TextBox1.Text += "9";

            }
            else
            {
                TextBox1.Text += " 9";
            }
        }

        protected void Button15_Click(object sender, EventArgs e)
        {
            TextBox1.Text += " *";
        }

        protected void Button16_Click(object sender, EventArgs e)
        {
            TextBox1.Text += " -";
        }

        protected void Button17_Click(object sender, EventArgs e)
        {
            TextBox1.Text += " (";
        }

        protected void Button18_Click(object sender, EventArgs e)
        {
            TextBox1.Text += " )";
        }

        protected void Button19_Click(object sender, EventArgs e)
        {
            TextBox1.Text += " +";
        }
    }
}